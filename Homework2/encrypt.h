//Header file for encrypt.c

int coprime(int x, int c);
int GCD(int a, int b);
int mod_inverse(int base, int m);
void myRSA();
int nthPrime(int n);
long encrypt(int chr, int key, int c);
long* encrypt_string(char* message, int e, int c);
int decrypt(int chr, int key, int c);
char* decrypt_string(long* encrypted, int e, int c, int num_bytes);