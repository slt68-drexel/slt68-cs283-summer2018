//Input and Output Handlers for threaded functions
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<string.h>
#include"handlers.h"
#include"encrypt.h"

//This function sends input through the socket
    //Reads in users input from stdin
    //Encrypts the input based on args passed
    //sends input down socket
void *sender(void *args){
    struct args *args1;
    args1 = (struct args*) args;
    int sock = args1->socket;
    int e = args1->val1;
    int c = args1->val2;
    char inMessage[1000];
    char *temp;
    long *encMessage;
    int buffer;

    while(1){
        fgets(inMessage, 1000, stdin);
        temp = strtok(inMessage, "\n");
        buffer = strlen(temp)*sizeof(long);
        encMessage = encrypt_string(temp, e, c);
        printf("ENCRYPTED %s to ",temp);
        for(int i =0; i<strlen(temp); i++){
            printf("%ld ",encMessage[i]);
        }
        printf("\n");
        send(sock, encMessage, buffer, 0);
    }

}
//This function receives message from the socket
    //Reads in data from the socket
    //Decrypts the date based on args passed
    //Prints out decrypted data
void *receiver(void *args){
    struct args *args2;
    args2 = (struct args*) args;
    int sock = args2->socket;
    int d = args2->val1;
    int dc = args2->val2;
    long inMessage[1000];
    char* decMessage;
    char* temp;

    while(recv(sock, inMessage, 1000, 0)>0){
        decMessage = decrypt_string(inMessage, d, dc, 1000);
        printf("Message Received: %s\n",decMessage);
    }

}