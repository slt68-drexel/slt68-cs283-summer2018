//Main Client File
    //-Attaches to a socket
    //-Multithreads I/O to socket

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "handlers.h"
  
int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    int e, c, d, dc, port;
    struct sockaddr_in serv_addr;

    //These structs hold the encryption/decryption values
        //As well as the port
    struct args *args1 = malloc(sizeof(struct args));
    struct args *args2 = malloc(sizeof(struct args));

    //Sifts through the args and assigns vals to appropriate structs
    if(argc < 6){
        printf("Missing args\n");
        exit(EXIT_FAILURE);
    }else {
        port = atoi(argv[1]);
        args1->val1 = atoi(argv[2]);
        args1->val2 = atoi(argv[3]);
        args2->val1 = atoi(argv[4]);
        args2->val2 = atoi(argv[5]);
    }

    //Assigns a socket descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    args1->socket = sock;
    args2->socket = sock;

    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
      
    //Attaches socket to local Inet
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    //Attempts to connect to the server through the socket
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    pthread_t inThread, outThread;

    //If the connection is made, the IO loop beings
        //Calls inThread and outThread threaded functions
            //They both operate through functions in the handler file
                //Args1 and Args2 contain the args from command line
    while(1){
        if(pthread_create (&inThread, NULL, sender, (void*)args1)<0){
            perror("sender thread");
            exit(EXIT_FAILURE);
        }
        if(pthread_create (&outThread, NULL, receiver, (void*)args2)<0){
            perror("receiver thread");
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}