//Main Server File
    //-Attaches to a socket
    //-Multithreads I/O to socket

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "handlers.h"


int main(int argc, char const *argv[])
{
    int server_fd, sock, valread;
    struct sockaddr_in address;
    int opt = 1, addrlen = sizeof(address);
    int e, c, d, dc, port;

    //These structs hold the encryption/decryption values
        //As well as the port
    struct args *args1 = malloc(sizeof(struct args));
    struct args *args2 = malloc(sizeof(struct args));

    //Sifts through the args and assigns vals to appropriate structs
    if(argc < 6){
        printf("Missing args\n");
        exit(EXIT_FAILURE);
    }else {
        port = atoi(argv[1]);
        args1->val1 = atoi(argv[2]);
        args1->val2 = atoi(argv[3]);
        args2->val1 = atoi(argv[4]);
        args2->val2 = atoi(argv[5]);
    }

    //Assigns a socket descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //Preps socket for port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( port );
      
    //Binds socket to port 
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    //Begins listening on socket
    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    pthread_t inThread, outThread;

    //Once the socket accepts a connection
        //Calls inThread and outThread threaded functions
            //They both operate through functions in the handler file
                //Args1 and Args2 contain the args from command line
    while (sock = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)){
        int pid;
        args1->socket = sock;
        args2->socket = sock;
        if(pthread_create (&inThread, NULL, sender, (void*)args1)<0){
            perror("sender thread");
            exit(EXIT_FAILURE);
        }
        if(pthread_create (&outThread, NULL, receiver, (void*)args2)<0){
            perror("receiver thread");
            exit(EXIT_FAILURE);
        }
    }
}