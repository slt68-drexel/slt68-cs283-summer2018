#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include "encrypt.h"

//This function is passed a string and encrypts it into a long*
	//That long* is sent down the socket	
long* encrypt_string(char* message, int e, int c){
	long* enc = malloc(strlen(message)*sizeof(long));
	for(int i=0; i < strlen(message); i++){
		enc[i] = encrypt(message[i], e, c);
	}
	return enc;
}

//This function is passed a long* and decrypts it into a string
	//The string is then printed to screen
char* decrypt_string(long* encrypted, int e, int c, int num_bytes){
	char* str = malloc(num_bytes*sizeof(char));
	for(int k = 0; k< num_bytes; k++){
		str[k] = decrypt(encrypted[k], e, c);
	}
	return str;
}

//This is a helper function for encrypt_string
	//encrypt_string passes this function each character to be encrypted
long encrypt(int chr, int key, int c){
	long int temp = (chr * chr) % c;
	for(int i = 0; i < key-2; i++){
		temp = (temp * chr) % c;
	}
	return temp;
}

//This is a helper function for decrypt_string
	//decrypt_string passes this function each character to decrypt
int decrypt(int chr, int key, int c){
	long int temp = (chr * chr) % c;
	for(int i = 0; i < key-2; i++){
		temp = (temp * chr) % c;
	}
	return temp;
}

//This function is passed an int n and finds the n'th prime
int nthPrime(int n){
	int i,j,isPrime,count=0;

    for(i=2;i<=1000000;i++)
    {
        isPrime=0;
		//Sqrt(i) will decrease computational time without hindering the result 
        for(j=2;j<=sqrt(i)+1;j++)
        {
            if(i%j==0)
            {
                isPrime=1;
                break;
            }
        }
        if(isPrime==0)
        {
            count++;
        }
        if(count==n)
        {
            return i;
            break;
        }
    }
}

//Given two integers, this function will find a co-prime to those integers
int coprime(int m,int c){
	srand(time(NULL));
	int x = rand() % 100;
	while((GCD(x, m) != 1) || (GCD(x, c) != 1)){
		x++;
	}
	return x;
}

//This function will return the greatest common denominator of two integers
int GCD(int a, int b){
	if(b==0){
		return a;
	} else {
		return GCD(b, a % b);
	}
}

//This function serves to generate a public/private key combo
	//First is finds two random primes
	//Finds the co-prime of those
	//Finds the mod inverse of the co-prime and a prime
	//Prints the keys
void myRSA(){
	srand(time(NULL));
	int x = rand() % 20;
	int y = rand() % 20;
	while(x==y){
		y = rand() % 20;
	}
	int a = nthPrime(x);
	int b = nthPrime(y);
	int c = a * b;
	int m = (a - 1) * (b - 1);
	int e = coprime(m, c);
	int d = mod_inverse(e, m);
	int* keys = malloc(3*sizeof(int));
	printf("public key is (%d,%d)\n", e, c);
	printf("private key is (%d,%d)\n", d, c);
	
}

//Mod inverse returns base^-1 mod m
int mod_inverse(int base, int m){
	base = base % m;
	for(int i = 1; i < m; i++){
		if((base * i) % m == 1){
			return i;
		}
	}
}