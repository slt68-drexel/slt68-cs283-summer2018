Student Info:
Adam Mattaway(aam392)
Sam Truyen(slt68)

To Run:
	To get your keys run `make genKey` this will print your public and private
	keys. Do this on the client and sever side so that you have two keys. To run 
	the server first enter `PORT = P E=e C=c D=d DC=dc` where P is some random 
	port you wish to run the server on and e,c is your partners public key and 
	d,dc is your private key. Now enter `export PORT E C D DC` since make will 
	spawn a child to run the code we need them to be enviornment variables. To 
	run the client it is largely the same but the parameter values will be different.
	use the server's public key for e,c and the client's private key for d,dc. Now
	you should be good to start chatting with yourself!
	
Included Files:
	client.c
	encrypt.c, encrypt.h
	server.c
	handlers.c, handlers.h
	genKey.c
	Makefile
	README
	
P.S.
	Our M and N for primes use random numbers from 0 to 20 and don't require user input
	also we didn't implement key cracker 