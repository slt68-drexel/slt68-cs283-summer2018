//Header file for handlers.c

//This struct holds the args passed in command line
struct args{
    int socket;
    int val1;
    int val2;
};

void *sender(void *args);
void *receiver(void *args);