#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* thousand_count(void* vargp){
	volatile int* total = (volatile int*)vargp;
	for(int i = 0; i < 1000; i++){
		(*total)++;
	}
}
void* thousand_count_inner_lock(void* vargp){
	volatile int* total = (volatile int*)vargp;
	for(int i = 0; i < 1000; i++){
		pthread_mutex_lock(&mutex);
		(*total)++;
		pthread_mutex_unlock(&mutex);
	}
}
void* thousand_count_outer_lock(void* vargp){
	volatile int* total = (volatile int*)vargp;
	pthread_mutex_lock(&mutex);
	for(int i = 0; i < 1000; i++){
		(*total)++;
	}
	pthread_mutex_unlock(&mutex);
}
int average_count(int* lis, int items){
	int total = 0;
	for(int i = 0; i < items; i++){
		total += lis[i];
	}
	int average = total / items;
	return average;
}
void no_locks(int num){
	
	clock_t start = clock();
	int* totals = malloc(10*sizeof(int));
	for(int i = 0; i < 10; i++){
		volatile int total = 0;
		pthread_t* ids = malloc(num*sizeof(pthread_t));
		for(int j = 0; j < num; j++){
			pthread_create(&(ids[j]), NULL, thousand_count, (void *)&total);
		}
		for(int k = 0; k < num; k++){
			pthread_join(ids[k], NULL);
		}
		totals[i] = total;
	}
	clock_t end = clock() - start;
	printf("Average runtime of no locks in microseconds : %ld\n", end/10);
	int average = average_count(totals, 10);
	printf("%s : %d\n", "Average final count no locks", average);
}
void internal_locks(int num){
	clock_t start = clock();
	int* totals = malloc(10*sizeof(int));
	for(int i = 0; i < 10; i++){
		volatile int total = 0;
		pthread_t* ids = malloc(num*sizeof(pthread_t));
		for(int j = 0; j < num; j++){
			pthread_create(&(ids[j]), NULL, thousand_count_inner_lock, (void *)&total);
		}
		for(int k = 0; k < num; k++){
			pthread_join(ids[k], NULL);
		}
		totals[i] = total;
	}
	clock_t end = clock() - start;
	printf("Average runtime of internal locks in microseconds : %ld\n", end/10);
	int average = average_count(totals, 10);
	printf("%s : %d\n", "Average final count internal locks", average);
}
void external_locks(int num){
	clock_t start = clock();
	int* totals = malloc(10*sizeof(int));
	for(int i = 0; i < 10; i++){
		volatile int total = 0;
		pthread_t* ids = malloc(num*sizeof(pthread_t));
		for(int j = 0; j < num; j++){
			pthread_create(&(ids[j]), NULL, thousand_count_outer_lock, (void *)&total);
		}
		for(int k = 0; k < num; k++){
			pthread_join(ids[k], NULL);
		}
		totals[i] = total;
	}
	clock_t end = clock() - start;
	printf("Average runtime of external locks in microseconds : %ld\n", end/10);
	int average = average_count(totals, 10);
	printf("%s : %d\n", "Average final count external locks", average);
}
int main(int argc, char* argv[]){
	int num_threads = atoi(argv[1]);
	no_locks(num_threads);
	internal_locks(num_threads);
	external_locks(num_threads);
	return 0;
}