#include <stdlib.h>
#include <stdio.h>

int main(){	
	int N = 10;
	int i;
	
	int* ptr = (int*) malloc(N*sizeof(int));
	
	for(i = 0; i<N; i++){
		ptr[i] = i;
		printf("%d\n", ptr[i]);
	}
	
	free(ptr);
	
	return 0; 
}

