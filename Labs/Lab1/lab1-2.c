#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(){	
	int N = 10;
	int M = 15;
	int i,j;
	
	//initializes a two dimensional array of characters
	char** ptr = (char**) malloc(N*sizeof(char*));
	
	for(i = 0; i<N; i++){
		
		ptr[i] = (char*) malloc(M*sizeof(char));
		for(j = 0; j<M; j++){
			if(j==14){
				ptr[i][j]='\0';
			}
			else{
				ptr[i][j]='a';
			}
		}
		char* print = ptr[i];
		printf("%s\n", print);
	}
			
	return 0; 
}

