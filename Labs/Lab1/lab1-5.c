#include <stdlib.h>
#include <stdio.h>
#include <time.h>


struct Array {
	int* array;
	int size;
	int len;
};
//Initalizes a array to size of size
void setArray(struct Array* head,int size){
	head->array = (int*) malloc(size*sizeof(int));
	head->len = 0;
	head->size = size;

}
//add element to array and increase size by one if array length
//is equal to memory size
int addOne(struct Array* head, int element){
	if(head->len == head->size){
		head->size++;
		head->array = realloc(head->array, head->size*sizeof(int));
	}
	head->array[head->len++]=element;
}
//add element to array and double the memory size of the array if length
//is equal to memory size
int addDouble(struct Array* head, int element){
	if(head->len == head->size){
		head->size = head->size*2;
		head->array = realloc(head->array, head->size*sizeof(int));
	}
	head->array[head->len++]=element;
}
//if array is not null loop through and print elements
void printArray(struct Array* head){
	if(head == NULL){
		printf("Array is empty");
	}
	else{
		for(int i =0; i<head->len; i++){
			printf("%d:%d\n", i, head->array[i]);
		}
	}
}
//if array exists try to get element at given index 
int getElementByIndex(struct Array*  head, int index){
	if(head == NULL || head->len >= index){
		return NULL;
	}
	else{
		return head->array[index];
	}
}
//loop through array and delete and occurance of value by
//shifting all next values forward
void removeElementByValue(struct Array* head, int value){
	if(head !=NULL){
		for(int i = 0; i<head->len; i++){
			if(head->array[i] == value){
				for(int j = i; j<head->len; j++){
					head->array[j] = head->array[j+1];
					head->len--;
				}
			}
		}
	}
}

int main(){
	int N = 20;
	int NUM_ELEMENTS = 100000;
	clock_t begin, xDiff, yDiff;
	
	struct Array* x = (struct Array*) malloc(sizeof(struct Array));
	struct Array* y = (struct Array*) malloc(sizeof(struct Array));
	
	//initialize arrays
	setArray(y, N);
	setArray(x, N);
	
	//time adding 100000 elements to an array using addOne function and print result
	begin = clock();
	for(int i = 0; i<NUM_ELEMENTS; i++){
		addOne(x, i);
	}
	xDiff = clock() - begin;
	printf("Time to add %d elements by increasing size by one: \n %d microseconds\n",NUM_ELEMENTS, xDiff);

	//time adding 100000 elements to an array using addDouble function and print result
	begin = clock();
	for(int j = 0; j<NUM_ELEMENTS; j++){
		addDouble(y, j);
	}
	yDiff = clock() - begin;
	printf("Time to add %d elements by doubling the size: \n %d microseconds\n",NUM_ELEMENTS, yDiff);
	
	//Figure out which one was faster
	if(yDiff<xDiff){
		printf("%s\n %d microseconds\n", "Doubling was faster by:", xDiff - yDiff);
	} else if(xDiff<yDiff){
		printf("%s\n %d microseconds\n", "Adding one was faster by:", yDiff - xDiff);
	} else {
		printf("%s\n" ,"Theres was no difference");
	}

	//free pointers
	free(x->array);
	free(x);
	free(y->array);
	free(y);

	return 0;
}
