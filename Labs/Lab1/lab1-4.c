#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


struct Node{
        int data;
        struct Node* next;
    };

//loops through and prints a not null list else prints "List is empty"
void printLinkedList(struct Node* a)
{
	if(a == NULL){
		printf("%s\n", "List is empty \0");
	}
	else{
		while(a->next!=NULL){
			int cur = a->data;
			printf("%d\n", cur);
			a = a->next;
		}
		printf("%d\n", a->data);
	}
}

//Loop to check if list is sorted or not
bool sorted(struct Node* a){
	while(a->next!=NULL){
		if(a->data > a->next->data){
			return false;
		}
		a = a->next;
	}
	return true;
}

//I know bubblesort is ugly for linked lists but 
//it said to adapt my previous sort so here you have it
//my bubblesort for a singly linked list. Returns new head of list
struct Node* sortList(struct Node* head){
    if(head == NULL){
		return NULL;
	}
	else{
		struct Node *i, *j;
		struct Node* prev = NULL;
		
		i = head;
		while(!sorted(head) && i != NULL){
			i = head;
			prev = head;
			for(j = i->next; j!=NULL; j = j->next){
				if(i->data > j->data){
					i->next = j->next;
					j->next = i;
					if(i==head){
						head = j;
						prev = j;
					}
					else{
						prev->next = j;
						prev = j;
					}
					j = i;
				}
				else{
					i = j;
				}
			}
		}
		return head;
	}
}

int main(){
	
	//initialized list nodes
    struct Node* node1 = (struct Node*) malloc(sizeof(struct Node));
    struct Node* node2 = (struct Node*) malloc(sizeof(struct Node));
    struct Node* node3 = (struct Node*) malloc(sizeof(struct Node));
    struct Node* node4 = (struct Node*) malloc(sizeof(struct Node));
    struct Node* node5 = (struct Node*) malloc(sizeof(struct Node));
    
	//gave nodes data elements and linked them into an unsorted list
    node1->data=10;
    node1->next=node2;
    
    node2->data=4;
    node2->next=node3;
    
    node3->data=8;
    node3->next=node4;
    
    node4->data=2;
    node4->next=node5;
    
    node5->data=3;
    node5->next=NULL;

    printf("%s\n", "Linked list before sorting \0");
    printLinkedList(node1);
    
	
    struct Node* newHead = (struct Node*) sortList(node1);
	
	printf("%s\n", "Linked list after sorting \0");
	printLinkedList(newHead);
	
    return 0;
}
