#include <stdio.h>
#include <stdlib.h>

//bubblesort adapted to use pointer notation
int* my_sort(int* a, int size){
	int* temp = a;
	int i, j;
	for(i = 0; i<size; i++){
		for(j = 0; j<i; j++){
			if(temp[i]<temp[j]){
				int cur = temp[j];
				temp[j] = temp[i];
				temp[i] = cur;
			}
		}
	}
	return temp;
}
int main(){
	//initializes then list
	int* list = (int*) malloc(5*sizeof(int));
	int i;
	
	//gives each element a value
	list[0] = 89;
	list[1] = 123;
	list[2] = 222;
	list[3] = 4;
	list[4] = 9;
	
	printf("%s\n", "List before sorting\0");
	//print list values to the screen
	for(i = 0; i<5; i++){
		printf("%d\n", list[i]);
	}
	list = my_sort(list, 5);
	
	printf("%s\n", "List after sorting\0");
	for(i = 0; i<5; i++){
		printf("%d\n", list[i]);
	}
	return 0;
}
