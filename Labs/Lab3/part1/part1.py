import csv
import numpy as numpy
import util
from multiprocessing import Pool


def main():
    with open("DelayedFlights.csv") as csvfile:
        reader = csv.DictReader(csvfile)
        sequential(reader)
#         split(reader, 2)
#         split(reader, 4)
#         split(reader, 8)
#         split(reader, 16)

# def split(reader, n):
#     dataArr = []
#     for row in reader:
#         dataArr.append(row)
#     dataArr = numpy.array_split(dataArr,n)
#     cpuPool = Pool(PROCESSES = n)
#     mapped = cpuPool.map()



# def map(reader, lc):

def sequential(reader):
    avgDelay = {}
    countDelayComboVal = 0
    countDelayComboLabel = ""
    countDelayCarrierVal = 0
    countDelayCarrierLabel = ""
    countRow = 0
    for row in reader:
            tempWeather = row["WeatherDelay"]
            tempSecurity = row["SecurityDelay"]
            tempCD = row["CarrierDelay"]
            tempArrDelay = row["ArrDelay"]
            tempDepDelay = row["DepDelay"]
            if tempWeather or tempSecurity:
                tempInt = int(float(tempWeather))+int(float(tempSecurity))
                if (tempInt) > countDelayComboVal:
                    countDelayComboVal = tempInt
                    countDelayComboLabel = row["Origin"]
            if tempCD:
                tempInt = int(float(tempCD))
                if tempInt > countDelayCarrierVal:
                    countDelayCarrierVal = tempInt
                    countDelayCarrierLabel = row["UniqueCarrier"]
            if tempArrDelay and tempDepDelay:
                tempInt1 = int(float(tempArrDelay))
                tempInt2 = int(float(tempDepDelay))
                if row["Origin"] in avgDelay:
                    tempOrig = row["Origin"]
                    tempCarrier = row["UniqueCarrier"]
                    tempTotal = avgDelay[tempOrig][0]
                    tempCount = avgDelay[tempOrig][1]
                    tempTotal += (tempInt1 + tempInt2)
                    tempCount += 1
                    avgDelay[tempOrig] = (tempTotal,tempCount,tempCarrier)
                else:
                    tempOrig = row["Origin"]
                    tempCarrier = row["UniqueCarrier"]
                    tempTotal = (tempInt1 + tempInt2)
                    tempCount = 1
                    avgDelay[tempOrig] = (tempTotal,tempCount,tempCarrier)
    for k,v in avgDelay.items():
        tempAvg = v[0]/v[1]
        print ("Avergae Delay for (Airport/Carrier)",k,"/",v[2],"is: ",tempAvg)
    print ("Longest Weather/Security Delay: ",countDelayComboVal,"mins by Airport: ",countDelayComboLabel)
    print ("Longest Carrier Delay: ",countDelayCarrierVal,"mins by carrier: ",countDelayCarrierLabel)

if __name__ == '__main__':
    main()
