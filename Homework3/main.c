#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct move{
	int x;
	int y;
}Move;

char** initBoard(int dim);
void printBoard(int dim, char** board);
void deallocBoard(int dim, char** board);
char getChar(char** board, int x, int y);
Move* placePiece(char** board, int x, int y, char val);
void play(int dim, int players);
int checkWin(int dim, char** board, int print);
int checkFull(int dim, char** board);


void testPipe(){
	int pipefd[2], n;
	char buff[100];
	Move* move = malloc(sizeof(Move));
	move->x = 5;
	move->y = 6;
	if (pipe(pipefd) < 0) {
		perror ("pipe error");
	}
	printf ("readfd = %d, writefd = %d", pipefd[0], pipefd[1]);
	if (write(pipefd[1], "hello world\n", 12) != 12) {
		perror ("write error");
	}
	if ((n=read(pipefd[0], buff, sizeof(buff))) < 0) {
		perror ("read error");
	}
	write (1, buff, n);

}

int main(int argc, char* argv[]){
	int dim, players;
	if(argc >= 2){
		dim = atoi(argv[1]);
		players = atoi(argv[2]);
	} else {
		dim = 10;
		players = 5;
	}
	
	//testPipe();
	//char** board = initBoard(dim);
	play(dim, players);
	//deallocBoard(dim, board);
	
	return 0;
}

int checkFull(int dim, char** board){
	int total = 0;
	for(int i = 0; i < dim; i++){
		for(int j = 0; j < dim; j++){
			if(board[i][j]!='-'){
				total++;
			}
		}
	}
	if(total==(dim * dim)){
		return 0;
	} else {
		return 1;
	}
}

int checkWin(int dim, char** board, int print){
	char* column = malloc(sizeof(char) * dim);
	char* diagonal = malloc(sizeof(char) * dim);
	
	for(int i = 0; i < dim; i++){
		for(int j = 0; j < dim; j++){
			column[j] = board[i][j];
		}
		if((strstr(board[i],"bbbb") != NULL)|| (strstr(column,"bbbb") != NULL)){
			if(print == 1){
				printf("Blue wins\n");
			}
			return 0;
		} else if ((strstr(board[i],"rrrr") != NULL) || (strstr(column,"rrrr") != NULL)){
			if(print == 1){
				printf("Red wins\n");
			}
			return 0;
		}
	}
	if(print == 1){
		printf("Tie\n");
	}
	return 1;
}

void play(int dim, int players){
	
	int count = 0;
	int full = 0;
	char val;
	int check = 1;
	char buf[100];
	int isChild = 1;
	
	
	int fd[players][2][2];
	pid_t playerPid[players];
	
	srand(time(NULL));
	for(int m = 0; m < players; m++){
		char** board = initBoard(dim);
		
		pipe(fd[m][0]);
		
		pipe(fd[m][1]);
		
		
		if(isChild == 1){
			playerPid[m] = fork();
		}
		while(checkFull(dim, board)!= 0){
			if(playerPid[m] == 0){
				isChild = 0;
				int x = rand() % dim;
				int y = rand() % dim;
				close(fd[m][0][1]);
				close(fd[m][1][0]);
				int nbytes = read(fd[m][0][0], &buf, sizeof(Move));
				if(checkWin(dim, board, 0) == 0){
					break;
				}
				Move* parentMove = (Move *) buf;
				Move* lastMove = placePiece(board, parentMove->x, parentMove->y, 'r');
				
				if(checkWin(dim, board, 0) == 0){
					break;
				}
				
				Move* childMove = placePiece(board, x, y, 'b');
				while(childMove == NULL){
					childMove = placePiece(board, rand() % dim, rand() % dim, 'b');
				}
				write(fd[m][1][1], childMove, sizeof(Move));
				if(checkWin(dim, board, 0) == 0){
					break;
				}
				free(lastMove);
				free(childMove);
				
			} else {
				isChild = 1;
				int x = rand() % dim;
				int y = rand() % dim;
				Move* move = placePiece(board, x, y, 'r');
				if(checkWin(dim, board, 0) == 0){
					break;
				}
				while(move == NULL){
					move = placePiece(board, rand() % dim, rand() % dim, 'r');
				}
				close(fd[m][0][0]);
				close(fd[m][1][1]);
				write(fd[m][0][1], move , sizeof(Move));
				
				if(checkWin(dim,board, 0) == 0){
					break;
				}
				
				int nbytes = read(fd[m][1][0], &buf, sizeof(Move));
				Move* childMove = (Move *) buf;
				Move* lastMove = placePiece(board, childMove->x, childMove->y, 'b');
				if(checkWin(dim, board, 0) == 0){
					break;
				}
				free(move);
				free(lastMove);
			}
		}
		if(playerPid[m] != 0){
			checkWin(dim, board, 1);
			printBoard(dim, board);
		} else {
			exit(0);
		}
		deallocBoard(dim, board);
	}
	if(isChild == 1){
		int child_status;
		for(int i=0;i<players;i++){
			pid_t wpid = wait(&child_status);
			if (WIFEXITED(child_status)){
	            printf("Child %d terminated with exit status %d\n",
		           wpid, WEXITSTATUS(child_status));
	        } else {
	            printf("Child %d terminated abnormally\n", wpid);
			}
		}
	}
}

char** initBoard(int dim){
	char** board = malloc(sizeof(char*)*dim);
	for(int i = 0; i < dim; i++){
		board[i] = malloc(sizeof(char)*dim);
		for(int j = 0; j < dim; j++){
			board[i][j] = '-';
		}
	}
	return board;
}

void printBoard(int dim, char** board){
	for(int k = 0; k < dim; k++){
			printf(" ----- ");
	}
	printf("\n");
	for(int i = 0; i < dim; i++){
		for(int j = 0; j < dim; j++){
			char temp = board[i][j];
			printf(" | ");
			printf("%c", temp);
			printf(" | ");
		}
		printf("\n");
		for(int k = 0; k<dim; k++){
			printf(" ----- ");
		}
		printf("\n");
	}
}

void deallocBoard(int dim, char** board){
	for(int i = 0; i < dim; i++){
		free(board[i]);
	}
	free(board);
}

Move* placePiece(char** board, int x, int y, char val){
	Move* temp = malloc(sizeof(Move));
	if(board[y][x]=='-'){
		board[y][x] = val;
		temp->x = x;
		temp->y = y;
		return temp;
	} else {
		return NULL;
	}
	free(temp);
}

char getChar(char** board, int x, int y){
	printf("%c\n", board[y][x]);
	return board[y][x];
}