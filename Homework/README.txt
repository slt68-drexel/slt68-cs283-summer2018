Compile:
	gcc -o file.o main.c or make run
Run:
	./file.o -d database or make run 
	Must use the -d flag followed by the name of database you 
	want to use
Quit:
	After it has started running type in quit and hit enter to 
	stop the program

Test:
	So I couldn't figure out how to make the test cases run through
	the makefile as my program execute won't stop unless quit is entered.
	I apologize that tests must be entered manually but I have written out
	tests below to copy and paste. 
	
	CREATE:
		CREATE TABLE test FIELDS[field1,field2]
	INSERT: 
		INSERT INTO test(field2=value2,field1=value1)
	SELECT:
		SELECT * FROM test WHERE(field1=value1)
	UPDATE:
		UPDATE test SET field2=value3 WHERE field1=value1
	DELETE:
		DELETE FROM test WHERE(field1=value1)
	DROP:
		DROP TABLE test
General Form of Commands:
	CREATE:
		CREATE TABLE tablename FIELDS[field1,field2,..]
	INSERT: 
		INSERT INTO tablename(field2=value2,field1=value1...)
	SELECT:
		SELECT * FROM  tablename WHERE(field1=value1)
	UPDATE:
		UPDATE tablename SET field1=value1 WHERE field2=value2
	DELETE:
		DELETE FROM tablename WHERE(field1=value1)
	DROP:
		DROP TABLE test
Notes:
	I also realize that I don't free some things that I malloc, I tried making a 
	function called my_free to free my linked lists but couldn't get it working in
	time. 
	