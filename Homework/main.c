#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

struct Node{
	char* field;
	char* data;
	struct Node* next;
};


void initialize();
void listDatabases();
struct Node* get_fields(char* path);
char* create_database(char* name);
void listElements(struct Node* head);
int get_num_commas(char* str);
int get_index(char* file, char* entry);
char* update_row(char* file, char* line, char* value, char* field, FILE* fp);

char* update_row(char* file, char* line, char* value, char* field, FILE* fp){
	/*
		helper for my_update that returns a string of the new row to put
		uses a linked list retrieved from get_fields to decide where a 
		value must go in the row.
	*/
	struct Node* fields = malloc(sizeof(struct Node));
	fields = (struct Node*) get_fields(file);
	struct Node* head = fields;
	int num_commas = get_num_commas(line);
	char temp[256];
	strcpy(temp, line);
	char* line_val = strtok(temp, ",");
	num_commas--;
	
	struct Node* cur = head;
	//use a sorted linked list of fields to store data
	while(line_val != NULL){
		if(strcmp(cur->field, field)==0){
			cur->data = value;
		} else {
			cur->data = line_val;
		}
		if(num_commas>0){
			line_val = strtok(NULL, ",");
			num_commas--;
		} else {
			line_val = strtok(NULL, "\n");
		}
		cur = cur->next;
	}
	char* newLine = malloc(sizeof(char)*256);
	num_commas = get_num_commas(line);
	struct Node* i;
	for(i = head; i->next!= NULL; i = i->next){
		strcpy(newLine, i->data);
		strcat(newLine, ",");
		fputs(newLine, fp);
	}
	strcpy(newLine, i->data);
	strcat(newLine, "\n");
	fputs(newLine, fp);
	return newLine;
}
int get_index(char* file, char* field){
	/*
		takes in a file name and field as strings and gets a linked
		list of the file's fields using get_fields and returns an integer
		of the position of the supplied field in that linked list 
	*/
	struct Node* fields = malloc(sizeof(struct Node));
	fields = (struct Node*) get_fields(file);
	struct Node* head = fields;
	
	int index = 0;
	while(head!=NULL){
		if(strcmp(head->field, field)==0){
			break; 
		} else {
			head = head->next;
			index++;
		}
		
	}
	return index;
}
int get_num_commas(char* str){
	/*
		takes in any string as input and counts the number of commas
		encountered until the new line character. returns the number
		of commas.
	*/
	int num_commas = 0;
	for(int i=0; str[i]!='\n'; i++){
		if(str[i]==','){
			num_commas++;
		}
	}
	return num_commas;
}
void listElements(struct Node* head){
	/*
		a helper function to list the field values of any given linked
		list and prints them
	*/
	struct Node* cur = head;
	while(cur->next!=NULL){
		printf("%s\n", cur->field);
		cur = cur->next;
	}
	printf("%s\n", cur->field);
}
void check_string(char* string){
	/*
		if a string contains a '~' '/' or '.' exit the program so user 
		cannot accidentaly delete their own files
	*/
	for(int i = 0; string[i] != 0; i++){
		if(string[i]=='.' || string[i]=='~' || string[i]=='/'){
			printf("Symbols '.', '~', '/' are not allowed\n");
			exit(1);
		}
	}
	
}
void my_select(char* path){
	/*
		Uses get_index to figure out the index in the line that we want to check,
		uses that value to strtok and get the correct column to check. If it matches
		the value strtok'ed from user input, print it.
	*/
	
	char* token = strtok(NULL, " ");
	char* from = strtok(NULL, " ");
	char* tableName = strtok(NULL, " "); 
	char* where = strtok(NULL, "(");
	char* field = strtok(NULL, "=");
	char* value = strtok(NULL, ")");
	
	if(token != NULL && tableName != NULL && field != NULL && value != NULL){
		char file[512];
		char line[256];
		
		strcpy(file, path);
		strcat(file, "/");
		strcat(file, tableName);
		strcat(file, ".csv");
		
		int index = get_index(file, field);
		
		char temp[256];
		FILE* fp;
		fp = fopen(file, "r");
		fgets(file, 256, fp);
		while(fgets(line, 256, fp) != NULL){
			int num_commas = get_num_commas(line);
			strcpy(temp, line);
			char* rowValue = strtok(temp, ",");
			num_commas--;
			for(int i = 0; i<index; i++){
				if(num_commas>0){
					rowValue = strtok(NULL, ",");
					num_commas--;
				} else {
					rowValue = strtok(NULL, "\n");
				}
			}
			if(strcmp(value, rowValue)==0){
				printf("%s\n", line);
			}
		}
		fclose(fp);
	} else {
		printf("Expected select in the form:\n SELECT * FROM tablename WHERE(field=value)\n");
	}
}
void my_update(char* path){
	/* 
		First read in from selected table and use update_row to get
		new string of any row that matches user input and write it 
		to a .tmp file. Then Read line for line from the .tmp file 
		into the original table.
	*/
	char* tableName = strtok(NULL, " ");
	char* set = strtok(NULL, " ");
	char* field1 = strtok(NULL, "=");
	char* value1 = strtok(NULL, " ");
	char* where = strtok(NULL, " ");
	char* field2 = strtok(NULL, "=");
	char* value2 = strtok(NULL, "\n");
	
	if(tableName != NULL && field1 != NULL && value1 != NULL && field2 != NULL && value2 != NULL){
		char file[512];
		char tmpFile[512];
		char line[256];
		
		strcpy(file, path);
		strcat(file, "/");
		strcat(file, tableName);
		strcat(file, ".csv");
		
		strcpy(tmpFile, file);
		strcat(tmpFile, ".tmp");
		
		int index = get_index(file, field2);
		char temp[256];
		
		FILE* fp;
		FILE* tmp;
		fp = fopen(file, "r");
		tmp = fopen(tmpFile, "w");
		while(fgets(line, 256, fp) != NULL){
			int num_commas = get_num_commas(line);
			strcpy(temp, line);
			char* rowValue = strtok(temp, ",");
			num_commas--;
			for(int i = 0; i<index; i++){
				if(num_commas>0){
					rowValue = strtok(NULL, ",");
					num_commas--;
				} else {
					rowValue = strtok(NULL, "\n");
				}
			}
			if(strcmp(value2, rowValue)==0){
				update_row(file, line, value1, field1, tmp); 
			} else {
				fputs(line, tmp);
			}
		}
		fclose(tmp);
		fclose(fp);
		
		fp = fopen(file, "w");
		tmp = fopen(tmpFile, "r");
		while(fgets(line, 256, tmp) != NULL){
			fputs(line, fp);
		}
		fclose(tmp);
		fclose(fp);
	} else {
		printf("Expected Update of the form:\n UPDATE tablename SET field1=value WHERE field2=value2\n");
	}
}
void append(struct Node* head, char* field){
	/* 
		Append a field to a linked list using malloc
	*/
	struct Node* cur = head;
	while(cur->next!=NULL){
		cur = cur->next;
	}
	struct Node* new = (struct Node*) malloc(sizeof(struct Node));
	new->field = field;
	new->data=NULL;
	new->next = NULL;
	cur->next = new;
}
void my_drop(char* path){
	/*
		Attempt to remove file with user input tablename and check 
		the string so as to no delete anything we don't want to,
	*/
	char* token = strtok(NULL, " ");
	char* tableName = strtok(NULL, " \n");
	if(tableName != NULL){
		check_string(tableName);
		
		strcat(path, "/");
		strcat(path, tableName);
		strcat(path, ".csv");
		
		remove(path);
	} else {
		printf("Expected drop of the form:\n DROP TABLE tablename\n");
	}
}
void my_insert(char* path){
	/*
		Use a linked list of the fields which are sorted and 
		for each entry loop through the linked list, if the node's
		field matches entry set it's data to the value
	*/
	char* into = strtok(NULL, " ");
	char* tableName = strtok(NULL, "("); 
	char* entries = strtok(NULL, ")");
	if(tableName != NULL && entries != NULL){
		char file[512];
		
		
		strcpy(file, path);
		strcat(file, "/");
		strcat(file, tableName);
		strcat(file, ".csv");
		
		struct Node* head = (struct Node*) malloc(sizeof(struct Node));
		struct Node* fields = (struct Node*) get_fields(file);
		
		
		head->field=NULL;
		head->next=NULL;
		
		char* entry = strtok(entries, "=");
		char* value = strtok(NULL, ",");
		
		while(entry != NULL){
			for(struct Node* temp = fields; temp!=NULL; temp = temp->next){
				if(strcmp(temp->field, entry)==0){
					temp->data = value;
				}
				else if(temp->data==NULL){
					temp->data = " ";
				}
			}
			entry = strtok(NULL, "=");
			value = strtok(NULL,",");
		}
		FILE* fp;
		fp = fopen(file, "a");
		char row[512];
		struct Node* temp;
		for(temp = fields; temp->next!=NULL; temp = temp->next){
			strcpy(row, temp->data);
			strcat(row, ",");
			fputs(row, fp);
		}
		strcpy(row, temp->data);
		strcat(row, "\n");
		fputs(row, fp);
		fclose(fp);
	} else {
		printf("Error: expected form 'INSERT INTO tableName(Field1=value,Field2=value,...)\n");
	}
}
struct Node* get_fields(char* path){
	/* 
		Open a file and create a linked list of it's fields or first row
	*/
	char fields[512];
	FILE* fp;
	fp = fopen(path, "r");
	fgets(fields, 512, fp);
	fclose(fp);
	
	int num_commas = get_num_commas(fields);
	
	char* field = strtok(fields, ",");
	num_commas--;
	struct Node* head = (struct Node*) malloc(sizeof(struct Node));
	
	
	head->field=NULL;
	head->next=NULL;
	head->data=NULL;
	
	while(field!=NULL){
		if(head->field == NULL){
			head->field=field;
		}
		else{
			append(head, field);
		}
		if(num_commas>0){
			field = strtok(NULL, ",");
			num_commas--;
		}
		else{
			field = strtok(NULL, "\n");
		}
	}
	return head;
}
void my_free(struct Node* head){
	
	while(head->next!=NULL){
		struct Node* cur = head->next;
		struct Node* prev = head;
		while(cur->next!=NULL){
			cur = cur->next;
			prev = prev->next;
		}
		free(cur);
	}
	free(head);
}
void my_create(char* path){
	/*
		Create a table with fields in the order that they are given by user
		Tables are stored as .csv files
	*/
	char* token = strtok(NULL, " ");
	char* tableName = strtok(NULL, " ");
	char* trash = strtok(NULL, "[");
	char* fields = strtok(NULL, "]");
	char* field = strtok(fields, ",");
	char file[512];
	
	if(tableName != NULL && field !=NULL){
		check_string(tableName);
		
		struct Node* head = (struct Node*) malloc(sizeof(struct Node));
		
		head->field=NULL;
		head->next=NULL;
		
		while(field != NULL){
			if(head->field==NULL){
				head->field=field;
			}
			else{
				append(head, field);
			}
			field = strtok(NULL, ",");
		}
		strcpy(file, path);
		
		strcat(file, "/");
		strcat(file, tableName);
		strcat(file, ".csv");
		
		FILE* fp;
		fp = fopen(file, "w");
		
		struct Node* cur = head;
		char temp[512];
		
		if(fp!=NULL){
			for(cur = head; cur->next!=NULL; cur = cur->next){
				strcpy(temp, cur->field);
				strcat(temp, ",");
				fputs(temp, fp);
			}
			strcat(cur->field, "\n");
			fputs(cur->field, fp);
			fclose(fp);
		}
	}
}
void my_delete(char* path){
	/* 
		similiar to update read in a file line by line and use index to get 
		the value from the correct column and if it doesn't match then write it
		to a .tmp file. Then read line for line from .tmp in the original table
	*/
	char* from = strtok(NULL, " ");
	char* tableName = strtok(NULL, " ");
	char* where = strtok(NULL, "(");
	char* field = strtok(NULL, "=");
	char* value = strtok(NULL, ")");
	
	if(field != NULL && tableName != NULL && value != NULL){
		char file[512];
		char line[256];
		char tmpFile[512];
		
		strcpy(file, path);
		strcat(file, "/");
		strcat(file, tableName);
		strcat(file, ".csv");
		
		strcpy(tmpFile, file);
		strcat(tmpFile, ".tmp");
		
		int index = get_index(file, field);
		
		char temp[256];
		FILE* fp;
		FILE* tmp;
		fp = fopen(file, "r");
		tmp = fopen(tmpFile, "w");
		fgets(line, 256, fp); //To ignore the first line
		while(fgets(line, 256, fp) != NULL){
			int num_commas = get_num_commas(line);
			strcpy(temp, line);
			char* rowValue = strtok(temp, ",");
			num_commas--;
			for(int i = 0; i<index; i++){
				if(num_commas>0){
					rowValue = strtok(NULL, ",");
					num_commas--;
				} else {
					rowValue = strtok(NULL, "\n");
				}
			}
			printf("%s,%s\n", value, rowValue);
			if(strcmp(value, rowValue)!=0){
				fputs(line, tmp);
			}
		}
		fclose(fp);
		fclose(tmp);
		
		fp = fopen(file, "w");
		tmp = fopen(tmpFile, "r");
		while(fgets(line, 256, tmp) != NULL){
			fputs(line, fp);
		}
		fclose(tmp);
		fclose(fp);
	}
}
char* create_database(char* name){
	check_string(name);
	int fd, retval;
	char* path = malloc(50*sizeof(char));
	strcpy(path, "./databases/");
	strcat(path, name);
	DIR* database_dir = opendir(path);
	if(database_dir == NULL){
		mkdir(path, 777);
	}
	closedir(database_dir);
	return path;
}
int main(int argc, char* argv[]){
	char* path;
	if(argc >= 2){
		if(strcmp(argv[1], "-d")==0){
			char* name = argv[2];
			path = create_database(name);
		}
		else{
			printf("Didn't understand arguments");
			exit(1);
		}
	}
	char buf[512];
	initialize(); //creates databases folder that will house all other databases as sub directories
	fgets(buf, 512, stdin);
	
	while(strcmp(buf, "quit\n") != 0){
		char* name = argv[2];
		path = create_database(name);
		char* token = strtok(buf, " ");
		if(strcmp(token, "CREATE")==0){
			my_create(path);
		}
		if(strcmp(token, "DROP")==0){
			my_drop(path);
		}
		if(strcmp(token, "INSERT")==0){
			my_insert(path);
		}
		if(strcmp(token, "SELECT")==0){
			my_select(path);
		}
		if(strcmp(token, "UPDATE")==0){
			my_update(path);
		}
		if(strcmp(token, "DELETE")==0){
			my_delete(path);
		}
		fgets(buf, 512, stdin);
	}
 	return 0;
}
void initialize(){
	DIR* database_dir = opendir("./databases");
	if(database_dir == NULL){
		mkdir("./databases", 777);
	}
	closedir(database_dir);
}
void listDatabases(){
	DIR* databases;
	struct dirent* dir;
	dir = opendir("./databases");
	if(databases){
		while((dir = readdir(databases)) != NULL){
			printf("%s\n", dir->d_name);
		}
		closedir(databases);
	}
}
